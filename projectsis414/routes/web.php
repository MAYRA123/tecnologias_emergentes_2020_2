<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('auth')->group(function(){
    Route::get("init","App\\Http\\Controllers\\AuthController@init");//pedimos informacion del servidor. de la clase authcontroller debo utilizar la funcion init
    Route::post("logout","App\\Http\\Controllers\\AuthController@logout");//de la clase authcontroller debo utilizar la funcion signup
    Route::post("signin","App\\Http\\Controllers\\AuthController@signin");//registramos informacion hacia el servidor de la clase authcontroller debo utilizar la funcion signin
    Route::post("signup","App\\Http\\Controllers\\AuthController@signup");//de la clase authcontroller debo utilizar la funcion signup
});
///crear un ruta globlar 