<!DOCTYPE html>
<html lang="es">
<head>
    <title>Tecnologias emergentes</title>
   <meta charset="UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

    </div>
    <script src="{!! asset('js/app.js')!!}"></script>
</body>
</html>